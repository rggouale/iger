import './style/index.css'
import './style/tailwind.css'
import './components/CallPopup/CallPopup.css'
import App from './components/app'

export default App