import tailwind from "preact-cli-tailwind"

export default (config, env, helpers) => {
    config = tailwind(config, env, helpers)
    return config
}